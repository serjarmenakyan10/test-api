<?php

namespace App\Traits;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Response;
use Illuminate\Validation\ValidationException;

trait FormRequestTrait
{
    protected function failedValidation(Validator $validator)
    {
        $errors['validation'] = (new ValidationException($validator))->errors();
        throw new HttpResponseException(
           response()->json($errors, JsonResponse::HTTP_UNPROCESSABLE_ENTITY));
    }
}