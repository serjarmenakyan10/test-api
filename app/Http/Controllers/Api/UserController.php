<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\StoreUser;
use App\Http\Requests\UpdateUser;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Create user
     *
     * @param StoreUser $request
     * @return \Illuminate\Http\JsonResponse
     *
     */
    public function create(StoreUser $request)
    {
        $user = User::create([
            'name' => $request->name,
            'email' =>$request->email,
            'password' => Hash::make($request->password),
        ]);

        return response()->json($user);
    }

    /**
     * Update User
     *
     * @param User $user
     * @param UpdateUser $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(User $user, UpdateUser $request)
    {
        $user->name = $request->name;
        $user->email = $request->email;
        if (isset($request->password) && strlen($request->password) >= 8){
            $user->password = $request->password;
        }
        $user->save();

        return response()->json($user);
    }

    /**
     * Delete user
     *
     * @param User $user
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function delete(User $user)
    {
        $user->delete();

        return response()->json('User Removed');
    }

    /**
     * @param User $user
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addToTeam(User $user, Request $request)
    {
        $user->teams()->sync($request->team_id);

        return response()->json('User assigned to team');
    }

    /**
     *
     *
     * @param User $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUserTeams(User $user)
    {
        return response()->json($user->teams);
    }

}
