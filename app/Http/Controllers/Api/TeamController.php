<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\Storeteam;
use App\Team;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class TeamController extends Controller
{
    /**
     * Create team
     *
     * @param StoreTeam $request
     * @return \Illuminate\Http\JsonResponse
     *
     */
    public function create(StoreTeam $request)
    {
        $team = Team::create([
            'name' => $request->name
        ]);

        return response()->json($team);
    }

    /**
     * Update team
     *
     * @param Team $team
     * @param StoreTeam $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Team $team, StoreTeam $request)
    {
        $team->name = $request->name;

        $team->save();

        return response()->json($team);
    }

    /**
     * Delete team
     *
     * @param Team $team
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function delete(Team $team)
    {
        $team->delete();

        return response()->json('Team Removed');
    }

    /**
     * Get team owner
     *
     * @param Team $team
     * @return \Illuminate\Http\JsonResponse
     */
    public function getOwner(Team $team)
    {
        return response()->json($team->owner);
    }

    /**
     * Set team owner
     *
     * @param Team $team
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function setOwner(Team $team, Request $request)
    {
        $team->user_id = $request->user_id;
        $team->save();

        return response()->json($team);
    }
}
