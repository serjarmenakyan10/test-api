<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class TeamTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function create()
    {
        $response = $this->json('POST', '/api/team/create', ['name' => 'Team 1']);

        $response
            ->assertStatus(201)
            ->assertExactJson([
                'id' => 1,
                'name' => 'Team 1'
            ]);
        $response->assertStatus(200);
    }

    public function update()
    {
        $response = $this->json('PUT', '/api/team/update/1', ['name' => 'Team new']);

        $response
            ->assertStatus(201)
            ->assertExactJson([
                'id' => 1,
                'name' => 'Team New'
            ]);
        $response->assertStatus(200);
    }

    public function delete()
    {
        $response = $this->json('delete', '/api/team/delete/1');

        $response
            ->assertStatus(201)
            ->assertExactJson([
                'Team Removed'
            ]);
        $response->assertStatus(200);
    }

    public function getOwner()
    {
        $response = $this->json('get', 'team/owner/get/1');

        $response
            ->assertStatus(201)
            ->assertExactJson([
                '{
                    "id": 1,
                    "name": "test",
                    "email": "serj@gmail.com",
                    "email_verified_at": null,
                    "created_at": "2019-04-09 12:15:50",
                    "updated_at": "2019-04-09 12:15:50"
                }'
            ]);
        $response->assertStatus(200);
    }
}
