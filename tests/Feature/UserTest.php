<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function create()
    {
        $response = $this->json('POST', '/api/user/create', ['name' => 'User 1', 'email' => 'test@test.com', 'password' => '123123123']);

        $response
            ->assertStatus(201)
            ->assertExactJson([
                'id' => 1,
                'name' => 'User 1',
                'email' => 'test@test.com',
            ]);
        $response->assertStatus(200);
    }

    public function update()
    {
        $response = $this->json('PUT', '/api/user/update/1', ['name' => 'User 1', 'email' => 'test@test.com', 'password' => '123123123']);

        $response
            ->assertStatus(201)
            ->assertExactJson([
                'id' => 1,
                'name' => 'User 1',
                'email' => 'test@test.com',
            ]);
        $response->assertStatus(200);
    }

    public function delete()
    {
        $response = $this->json('delete', '/api/user/delete/1');

        $response
            ->assertStatus(201)
            ->assertExactJson([
                'User Removed'
            ]);
        $response->assertStatus(200);
    }

    public function addToTeam()
    {
        $response = $this->json('post', '/api/user/team/add//1', ['team_id' => 1]);

        $response
            ->assertStatus(201)
            ->assertExactJson([
                'User assigned to team'
            ]);
        $response->assertStatus(200);
    }

    public function getUserTeams()
    {
        $response = $this->json('get', '/api/user/team/get/1');

        $response
            ->assertStatus(201)
            ->assertExactJson([
                    '{
                        "id": 1,
                        "name": "team1",
                        "user_id": 1,
                        "created_at": "2019-04-09 13:59:02",
                        "updated_at": "2019-04-09 15:02:02",
                        "pivot": {
                        "user_id": 2,
                            "team_id": 1
                        }
                    }'
                ]);
        $response->assertStatus(200);
    }
}
