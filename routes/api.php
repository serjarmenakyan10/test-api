<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::post('user/create','Api\UserController@create');
Route::put('user/update/{user}','Api\UserController@update');
Route::delete('user/delete/{user}','Api\UserController@delete');
Route::post('team/create','Api\TeamController@create');
Route::put('team/update/{team}','Api\TeamController@update');
Route::delete('team/delete/{team}','Api\TeamController@delete');
Route::get('team/owner/get/{team}','Api\TeamController@getOwner');
Route::post('team/owner/set/{team}','Api\TeamController@setOwner');
Route::post('user/team/add/{user}','Api\UserController@addToTeam');
Route::get('user/team/get/{user}','Api\UserController@getUserTeams');
